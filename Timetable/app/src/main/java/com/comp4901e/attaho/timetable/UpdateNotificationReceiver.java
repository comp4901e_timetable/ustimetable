package com.comp4901e.attaho.timetable;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Toast;

import com.comp4901e.attaho.timetable.data.DataController;
import com.comp4901e.attaho.timetable.fragments.TimetableFragment;

import java.util.Calendar;

/**
 * Created by attaho on 25/5/2016.
 */



public class UpdateNotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        new TimetableFragment();
        DataController dc = DataController.getInstance(null, null);
        String sectionNo = dc.getNextClass();
        if(sectionNo == "") return;

        String courseName = dc.getSectionDetailsMap().get(sectionNo).get("fullName").get(0);
        String courseCode = courseName.substring(0, courseName.indexOf("-") - 1);
        String location = dc.getSectionDetailsMap().get(sectionNo).get("room").get(0);
        if(location.contains("(")) {
            location = location.substring(0, location.indexOf("(")-1);
        }

        Intent intent2 = new Intent(context, MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 1, intent2, 0);

        Notification.Builder builder = new Notification.Builder(context);

        builder.setAutoCancel(false);
        builder.setTicker("");
        builder.setContentTitle("Your Next Class");
        builder.setContentText(courseCode);
        builder.setSubText(location);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setColor(context.getResources().getColor(R.color.material_drawer_primary));
        }
        builder.setSmallIcon(R.drawable.ic_directions_run_white_24dp);
        builder.setContentIntent(pendingIntent);
        builder.setOngoing(true);
        builder.build();

        Notification notification = builder.getNotification();
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(11, notification);

    }
}
