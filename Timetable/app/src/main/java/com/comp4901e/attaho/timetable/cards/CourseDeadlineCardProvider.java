package com.comp4901e.attaho.timetable.cards;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.comp4901e.attaho.timetable.R;

/**
 * Created by attaho on 21/5/2016.
 */
public class CourseDeadlineCardProvider extends CardProvider<CourseDeadlineCardProvider> {
    public int getDeadlineIndex() {
        return deadlineIndex;
    }

    int deadlineIndex = -1;

    public String getFullName() {
        return fullName;
    }

    String fullName = null;
    String name = null;

    public String getDate() {
        return date;
    }

    String date = null;

    public int getLayout() {
        return R.layout.course_deadline_view;
    }

    public CourseDeadlineCardProvider setDeadlineIndex(int deadlineIndex){
        this.deadlineIndex = deadlineIndex;
        notifyDataSetChanged();
        return this;
    }


    public CourseDeadlineCardProvider setFullName(String text){
        this.fullName = text;
        notifyDataSetChanged();
        return this;
    }

    public CourseDeadlineCardProvider setDeadlineName(String text){
        this.name = text;
        notifyDataSetChanged();
        return this;
    }

    public CourseDeadlineCardProvider setDate(String text){
        this.date = text;
        notifyDataSetChanged();
        return this;
    }

    @Override
    public void render(@NonNull View view, @NonNull Card card) {
        super.render(view, card);

        TextView textView;
        View layoutView;

        textView = (TextView) view.findViewById(R.id.deadlineName);
        textView.setText(name);

        textView = (TextView) view.findViewById(R.id.deadlineDate);
        textView.setText(date);
    }

}
