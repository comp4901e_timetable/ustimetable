package com.comp4901e.attaho.timetable.cards;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.comp4901e.attaho.timetable.R;
import com.mikepenz.iconics.view.IconicsTextView;

/**
 * Created by attaho on 21/5/2016.
 */
public class TimetableCardProvider extends CardProvider<TimetableCardProvider> {
    int iconColor = Color.GRAY;
    boolean isNextClass = false;
    String section;
    String dateTime;
    String location;

    public int getLayout() {
        return R.layout.timetable_card_view;
    }

    public TimetableCardProvider setOtherClass(){
        this.isNextClass = false;
        //notifyDataSetChanged();
        return this;
    }


    public TimetableCardProvider setNextClass(){
        this.isNextClass = true;
        //notifyDataSetChanged();
        return this;
    }

    public TimetableCardProvider setIconColor(int text){
        this.iconColor = text;
        notifyDataSetChanged();
        return this;
    }

    public TimetableCardProvider setSection(String text){
        this.section = text;
        notifyDataSetChanged();
        return this;
    }

    public TimetableCardProvider setDateTime(String text){
        this.dateTime = text;
        notifyDataSetChanged();
        return this;
    }

    public TimetableCardProvider setLocation(String text){
        this.location = text;
        notifyDataSetChanged();
        return this;
    }

    @Override
    public void render(@NonNull View view, @NonNull Card card) {
        super.render(view, card);

        TextView textView;

        textView = (TextView) view.findViewById(R.id.section);
        textView.setText(section);
        if(this.isNextClass) {
            textView.setTextColor(Color.WHITE);
        } else {
            textView.setTextColor(Color.BLACK);
        }

        textView = (TextView) view.findViewById(R.id.dateTime);
        textView.setText(dateTime);
        if(this.isNextClass) {
            textView.setTextColor(Color.WHITE);
        } else {
            textView.setTextColor(Color.BLACK);
        }

        textView = (TextView) view.findViewById(R.id.location);
        textView.setText(location);
        if(this.isNextClass) {
            textView.setTextColor(Color.WHITE);
        } else {
            textView.setTextColor(Color.BLACK);
        }

        if(this.isNextClass) {
            IconicsTextView icon;
            icon = (IconicsTextView)view.findViewById(R.id.sectionIcon);
            icon.setTextColor(Color.WHITE);
            icon = (IconicsTextView)view.findViewById(R.id.dateTimeIcon);
            icon.setTextColor(Color.WHITE);
            icon = (IconicsTextView)view.findViewById(R.id.locationIcon);
            icon.setTextColor(Color.WHITE);

            View nextClassView = (View)view.findViewById(R.id.nextClass);
            nextClassView.setVisibility(View.VISIBLE);

            View timetableCardView = (View)view.findViewById(R.id.timetableCardView);
            timetableCardView.setTag("nextClass");
        } else {
            IconicsTextView icon;
            icon = (IconicsTextView)view.findViewById(R.id.sectionIcon);
            icon.setTextColor(iconColor);
            icon = (IconicsTextView)view.findViewById(R.id.dateTimeIcon);
            icon.setTextColor(iconColor);
            icon = (IconicsTextView)view.findViewById(R.id.locationIcon);
            icon.setTextColor(iconColor);

            View nextClassView = (View)view.findViewById(R.id.nextClass);
            nextClassView.setVisibility(View.GONE);

            View timetableCardView = (View)view.findViewById(R.id.timetableCardView);
            timetableCardView.setTag("normal");
        }

        IconicsTextView sectionIcon = (IconicsTextView)view.findViewById(R.id.sectionIcon);;
        if(section.contains("LA")) {
            sectionIcon.setText("{gmd-desktop-windows}");
        } else if(section.contains("L")) {
            sectionIcon.setText("{gmd-record-voice-over}");
        } else if(section.contains("T")) {
            sectionIcon.setText("{gmd-description}");
        } else if(section.contains("R")) {
            sectionIcon.setText("{gmd-book}");
        }

    }

}
