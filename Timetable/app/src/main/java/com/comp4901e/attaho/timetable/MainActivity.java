package com.comp4901e.attaho.timetable;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import com.comp4901e.attaho.timetable.data.DataController;
import com.comp4901e.attaho.timetable.fragments.AddCoursesFragment;
import com.comp4901e.attaho.timetable.fragments.CourseFragment;
import com.comp4901e.attaho.timetable.fragments.DeadlinesFragment;
import com.comp4901e.attaho.timetable.fragments.SettingsFragment;
import com.comp4901e.attaho.timetable.fragments.TimetableFragment;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    static final String STATE_FRAGMENT = "fragmentState";
    private HashMap<String, Object> fragmentMap = new HashMap<String, Object>();
    private String selected = "timetable";
    private Drawer drawer;
    private Notification notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        Locale.setDefault(Locale.US);

        // Initialize fragments
        fragmentMap.put("timetable", new TimetableFragment());
        fragmentMap.put("deadlines", new DeadlinesFragment());
        fragmentMap.put("addCourses", new AddCoursesFragment());
        fragmentMap.put("settings", new SettingsFragment());

        // Load previously opened fragment
        if (savedInstanceState != null) {
            selected = savedInstanceState.getString(STATE_FRAGMENT);
        }

        // Load timetable fragment
        updateFragment();

        // Load data from internal storage
        DataController.getInstance(this, getWindow().getDecorView().getRootView()).loadData();

        // Load action bar
        Toolbar toolbar = (Toolbar) findViewById (R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create drawer object
        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        selected = drawerItem.getTag().toString();
                        updateFragment();
                        drawer.closeDrawer();
                        return true;
                    }
                })
                .build();

        updateDrawer();
        showNextClass();

//        Calendar cal = Calendar.getInstance();
//
//        Intent intent = new Intent(this, UpdateNotificationReceiver.class);
//        PendingIntent pintent = PendingIntent.getBroadcast(this, 0, intent, 0);
//
//        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        // schedule for every 30 seconds
//        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 1000, pintent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_actions, menu);
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){

        }
    }

    @Override
    public void onBackPressed() {
        View toolbar = findViewById(R.id.toolbar);
        toolbar.requestFocus();
        if (drawer != null && drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else if (findViewById(R.id.addCourseOverlay).getVisibility() == View.VISIBLE){
            DataController.getInstance(this, getWindow().getDecorView().getRootView())
                    .addOverlayClose(findViewById(R.id.addCourseOverlay));
        } else if (findViewById(R.id.addLinkOverlay).getVisibility() == View.VISIBLE) {
            DataController.getInstance(this, getWindow().getDecorView().getRootView())
                    .addOverlayClose(findViewById(R.id.addLinkOverlay));
        } else if (findViewById(R.id.addDeadlineOverlay).getVisibility() == View.VISIBLE) {
            DataController.getInstance(this, getWindow().getDecorView().getRootView())
                    .addOverlayClose(findViewById(R.id.addDeadlineOverlay));
        } else if (!selected.equals("timetable")){
            selected = "timetable";
            updateFragment();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putString(STATE_FRAGMENT, selected);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    public void updateFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if(selected.contains("-")) {
            CourseFragment cf = new CourseFragment();
            Bundle b = new Bundle();
            b.putString("courseName", selected);
            cf.setArguments(b);
            fragmentTransaction
                    .replace(R.id.fragment_container, cf, selected);
        } else {
            if(((Fragment)fragmentMap.get(selected)).isAdded()) {
                invalidateOptionsMenu();
                return;
            };
            fragmentTransaction
                    .replace(R.id.fragment_container, (Fragment) fragmentMap.get(selected), selected);
        }
        if(!DataController.getInstance(this, getWindow().getDecorView().getRootView()).isAdded()) {
            fragmentTransaction
                    .add(R.id.fragment_container, DataController.getInstance(this, getWindow().getDecorView().getRootView()), "DataController");
        }
        fragmentTransaction
                .commit();
        invalidateOptionsMenu();
    }

    public void updateDrawer() {
        drawer.removeAllItems();

        // Drawer Menu Items
        PrimaryDrawerItem timetable = new PrimaryDrawerItem()
                .withName(R.string.drawerTimetable)
                .withIcon(GoogleMaterial.Icon.gmd_event_note)
                .withTag("timetable");

        PrimaryDrawerItem deadlines = new PrimaryDrawerItem()
                .withName(R.string.drawerDeadlines)
                .withIcon(GoogleMaterial.Icon.gmd_access_alarm)
                .withTag("deadlines");

        PrimaryDrawerItem addCourses = new PrimaryDrawerItem()
                .withName(R.string.drawerAddCourses)
                .withIcon(GoogleMaterial.Icon.gmd_add)
                .withTag("addCourses");

        PrimaryDrawerItem settings = new PrimaryDrawerItem()
                .withName(R.string.drawerSettings)
                .withIcon(GoogleMaterial.Icon.gmd_settings)
                .withTag("settings");

        drawer.addItem(timetable);
        drawer.addItem(deadlines);
        drawer.addItem(new DividerDrawerItem());
        Set<String> coursesSet = DataController.getInstance(this, getWindow().getDecorView().getRootView()).getCurrentTermCourses();
        if(coursesSet != null) {
            for(String fullName : coursesSet) {
                String courseCode = fullName.substring(0, fullName.indexOf("-") - 1);
                drawer.addItem(
                        new PrimaryDrawerItem()
                                .withName(courseCode)
                                .withIcon(GoogleMaterial.Icon.gmd_school)
                                .withTag(fullName)
                );
            }
        }
        drawer.addItem(addCourses);
        drawer.addItem(new DividerDrawerItem());
        drawer.addItem(settings);
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public Drawer getDrawer() {
        return drawer;
    }

   public void showNextClass(){
       DataController dc = DataController.getInstance(this, getWindow().getDecorView().getRootView());
       String sectionNo = dc.getNextClass();
       if(sectionNo == "") return;

       String courseName = dc.getSectionDetailsMap().get(sectionNo).get("fullName").get(0);
       String courseCode = courseName.substring(0, courseName.indexOf("-") - 1);
       String location = dc.getSectionDetailsMap().get(sectionNo).get("room").get(0);
       if(location.contains("(")) {
           location = location.substring(0, location.indexOf("(")-1);
       }

       Intent intent = new Intent(this, MainActivity.class);

       PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 1, intent, 0);

       Notification.Builder builder = new Notification.Builder(MainActivity.this);

       builder.setAutoCancel(false);
       builder.setTicker("");
       builder.setContentTitle("Your Next Class");
       builder.setContentText(courseCode);
       builder.setSubText(location);
       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
           builder.setColor(getResources().getColor(R.color.material_drawer_primary));
       }
       builder.setSmallIcon(R.drawable.ic_directions_run_white_24dp);
       builder.setContentIntent(pendingIntent);
       builder.setOngoing(true);
       builder.build();

       notification = builder.getNotification();
       NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
       manager.notify(11, notification);
   }
}
