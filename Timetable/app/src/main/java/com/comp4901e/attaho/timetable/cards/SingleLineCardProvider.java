package com.comp4901e.attaho.timetable.cards;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.comp4901e.attaho.timetable.R;

/**
 * Created by attaho on 21/5/2016.
 */
public class SingleLineCardProvider extends CardProvider<SingleLineCardProvider> {

    String text = null;

    public int getLayout() {
        return R.layout.single_line_card_view;
    }

    public SingleLineCardProvider setText(String text){
        this.text = text;
        notifyDataSetChanged();
        return this;
    }

    @Override
    public void render(@NonNull View view, @NonNull Card card) {
        super.render(view, card);

        TextView textView;

        textView = (TextView) view.findViewById(R.id.text);
        textView.setText(text);
    }

}
