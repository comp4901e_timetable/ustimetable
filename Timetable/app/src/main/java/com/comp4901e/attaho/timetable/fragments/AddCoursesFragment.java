package com.comp4901e.attaho.timetable.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.comp4901e.attaho.timetable.data.DataController;
import com.comp4901e.attaho.timetable.R;

import org.neotech.library.retainabletasks.providers.TaskFragmentCompat;

/**
 * A placeholder fragment containing a simple view.
 */
public class AddCoursesFragment extends TaskFragmentCompat {

    public AddCoursesFragment() {
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Add Courses");
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.add_courses, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.course_add_actions, menu);
        DataController.getInstance(getActivity(), getView()).loadViews();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_update) {
            DataController.getInstance(getActivity(), getView()).updateCourses();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
