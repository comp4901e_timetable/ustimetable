package com.comp4901e.attaho.timetable.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class DataCourseTreeMap {
    public HashMap<String, TreeMap<String, HashMap<String, ArrayList<String> > > > getMap() {
        return map;
    }

    public void setMap(HashMap<String, TreeMap<String, HashMap<String, ArrayList<String> > > > map) {
        this.map = map;
    }

    private HashMap<String, TreeMap<String, HashMap<String, ArrayList<String> > > > map = null;
}
