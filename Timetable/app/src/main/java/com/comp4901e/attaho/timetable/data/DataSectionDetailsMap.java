package com.comp4901e.attaho.timetable.data;

import java.util.ArrayList;
import java.util.HashMap;

public class DataSectionDetailsMap {
    public HashMap<String, HashMap<String, HashMap<String, ArrayList<String> > > > getMap() {
        return map;
    }

    public void setMap(HashMap<String, HashMap<String, HashMap<String, ArrayList<String> > > > map) {
        this.map = map;
    }

    private HashMap<String, HashMap<String, HashMap<String, ArrayList<String> > > > map = null;
}
