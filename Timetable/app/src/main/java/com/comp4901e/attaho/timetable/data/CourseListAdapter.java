package com.comp4901e.attaho.timetable.data;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.comp4901e.attaho.timetable.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

/**
 * Created by attaho on 20/5/2016.
 */
public class CourseListAdapter extends ArrayAdapter<String> implements SectionIndexer {
    HashMap<String, Integer> mapIndex;
    String[] sections;
    String[] courses;

    public CourseListAdapter(Context context, String[] arr) {
        super(context, R.layout.course_list, arr);
        this.courses = arr;
        mapIndex = new LinkedHashMap<String, Integer>();

        for (int x = 0; x < courses.length; x++) {
            String fullName = courses[x];
            String dept = " " + fullName.substring(0, 4) + " ";

            // HashMap will prevent duplicates
            if(!mapIndex.containsKey(dept)) {
                mapIndex.put(dept, x);
            }
        }

        Set<String> sectionLetters = mapIndex.keySet();

        // create a list from the set to sort
        ArrayList<String> sectionList = new ArrayList<String>(sectionLetters);

        Collections.sort(sectionList);

        sections = new String[sectionList.size()];

        sectionList.toArray(sections);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        String fullName = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.course_list, parent, false);
        }
        // Lookup view for data population
        TextView courseCode = (TextView) convertView.findViewById(R.id.course_code);
        TextView courseName = (TextView) convertView.findViewById(R.id.course_name);
        // Populate the data into the template view using the data object
        courseCode.setText(fullName.substring(0, fullName.indexOf("-") - 1));
        courseName.setText(fullName.substring(fullName.indexOf("-") + 2));
        // Return the completed view to render on screen
        return convertView;
    }


    public int getPositionForSection(int section) {
        return mapIndex.get(sections[section]);
    }

    public int getSectionForPosition(int position) {
        for(int i = sections.length - 1; i >= 0; i--) {
            if(position > mapIndex.get(sections[i]))
                return i;
        }
        return 0;
    }

    public Object[] getSections() {
        return sections;
    }
}
