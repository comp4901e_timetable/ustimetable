package com.comp4901e.attaho.timetable.cards;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.comp4901e.attaho.timetable.R;

import java.util.ArrayList;

/**
 * Created by attaho on 21/5/2016.
 */
public class CourseDetailsCardProvider extends CardProvider<CourseDetailsCardProvider> {
    String fullName = null;
    String course = null;
    String lecture = null;
    String laboratory = null;
    String tutorial = null;
    String research = null;

    boolean isLoading = false;

    public int getLayout() {
        return R.layout.course_timetable_details;
    }

    public CourseDetailsCardProvider setFullName(String text){
        this.fullName = text;
        notifyDataSetChanged();
        return this;
    }

    public CourseDetailsCardProvider setCourse(String text){
        this.course = text;
        notifyDataSetChanged();
        return this;
    }

    public CourseDetailsCardProvider setLecture(String text){
        this.lecture = text;
        notifyDataSetChanged();
        return this;
    }

    public CourseDetailsCardProvider setLaboratory(String text){
        this.laboratory = text;
        notifyDataSetChanged();
        return this;
    }

    public CourseDetailsCardProvider setTutorial(String text){
        this.tutorial = text;
        notifyDataSetChanged();
        return this;
    }

    public CourseDetailsCardProvider setResearch(String text){
        this.research = text;
        notifyDataSetChanged();
        return this;
    }

    @Override
    public void render(@NonNull View view, @NonNull Card card) {
        super.render(view, card);

        TextView textView;
        View layoutView;

        textView = (TextView) view.findViewById(R.id.course);
        textView.setText(course);

        ArrayList<Object[]> viewList = new ArrayList<>();
        viewList.add(new Object[]{lecture, R.id.lecture, R.id.lectureLayout});
        viewList.add(new Object[]{laboratory, R.id.lab, R.id.labLayout});
        viewList.add(new Object[]{tutorial, R.id.tutorial, R.id.tutorialLayout});
        viewList.add(new Object[]{research, R.id.research, R.id.researchLayout});

        for(Object[] o : viewList) {
            layoutView = (View) view.findViewById((Integer) o[2]);
            textView = (TextView) view.findViewById((Integer) o[1]);
            if(((String)o[0]).length() > 0) {
                layoutView.setVisibility(View.VISIBLE);
                textView.setText(((String)o[0]));
            } else {
                layoutView.setVisibility(View.GONE);
            }
        }
    }

}
