package com.comp4901e.attaho.timetable.fragments;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.listeners.RecyclerItemClickListener;
import com.dexafree.materialList.view.MaterialListView;
import com.comp4901e.attaho.timetable.cards.SingleLineCardProvider;
import com.comp4901e.attaho.timetable.data.DataController;
import com.comp4901e.attaho.timetable.MainActivity;
import com.comp4901e.attaho.timetable.R;
import com.comp4901e.attaho.timetable.cards.CourseDeadlineCardProvider;
import com.comp4901e.attaho.timetable.data.DeadlineComparator;
import com.github.clans.fab.FloatingActionButton;

import org.neotech.library.retainabletasks.providers.TaskFragmentCompat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Set;

/**
 * A placeholder fragment containing a simple view.
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
public class DeadlinesFragment extends TaskFragmentCompat {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Deadlines");
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.deadlines, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        final DataController dc = DataController.getInstance(getActivity(), getView());
        MaterialListView mListView = (MaterialListView) getView().findViewById(R.id.deadline_listview);
        mListView.getAdapter().clearAll();

        if(dc.getCurrentTermCourses() != null) {
            SimpleDateFormat format = new SimpleDateFormat("d MMMM yyyy  h:mm a");
            for(String courseName : dc.getCurrentTermCourses()){
                String courseCode = courseName.substring(0, courseName.indexOf("-") - 1);
                int deadlineIndex = 0;
                ArrayList<Card> cardList = new ArrayList<>();
                for(Object o : dc.getUserDataMap().get(courseName).get("deadlines")) {
                    ArrayList<String> deadline = (ArrayList) o;
                    try {
                        if(format.parse(deadline.get(1)).before(new Date())) continue;
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Card deadlineCard = new Card.Builder(getActivity())
                            .withProvider(new CourseDeadlineCardProvider())
                            .setDeadlineIndex(deadlineIndex)
                            .setFullName(courseName)
                            .setDeadlineName(courseCode + " - " + deadline.get(0))
                            .setDate(deadline.get(1))
                            .endConfig()
                            .build();
                    deadlineCard.setTag(deadlineIndex + "courseDeadline");
                    cardList.add(deadlineCard);
                    deadlineIndex++;
                }
                Collections.sort(cardList, new DeadlineComparator());
                for(Card c : cardList) {
                    mListView.getAdapter().add(c);
                }
            }
        }

        if (mListView.getAdapter().getItemCount() == 0) {
            Card singleLineCard = new Card.Builder(getActivity())
                    .withProvider(new SingleLineCardProvider())
                    .setText("No Upcoming Deadlines")
                    .endConfig()
                    .build();
            singleLineCard.setTag("noDeadline");
            mListView.getAdapter().add(singleLineCard);
        }

        mListView.addOnItemTouchListener(new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(final Card view, int position) {
                String data = (String) view.getTag();
                if (data.contains("courseDeadline")) {
                    String courseName = ((CourseDeadlineCardProvider) view.getProvider()).getFullName();
                    dc.loadDeadlineEditPage(courseName, Integer.parseInt(data.substring(0, 1)));
                }
            }

            @Override
            public void onItemLongClick(final Card view, int position) {
                String data = (String) view.getTag();
                if (data.contains("courseDeadline")) {
                    String courseName = ((CourseDeadlineCardProvider) view.getProvider()).getFullName();
                    dc.loadDeadlineEditPage(courseName, Integer.parseInt(data.substring(0, 1)));
                }
            }
        });

        FloatingActionButton addDeadline = (FloatingActionButton) getView().findViewById(R.id.add_deadline_choose_course);
        addDeadline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Set<String> coursesSet = dc.getCurrentTermCourses();
                if (coursesSet != null) {
                    if (!coursesSet.isEmpty()) {
                        final ArrayList<String> listOfCourses = new ArrayList<String>();
                        ArrayList<String> listOfCourseCodes = new ArrayList<String>();
                        for (String fullName : coursesSet) {
                            String courseCode = fullName.substring(0, fullName.indexOf("-") - 1);
                            listOfCourseCodes.add(courseCode);
                            listOfCourses.add(fullName);
                        }
                        new MaterialDialog.Builder(getActivity())
                                .title("Choose Course")
                                .items(listOfCourseCodes)
                                .itemsCallback(new MaterialDialog.ListCallback() {
                                    @Override
                                    public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                        DataController.getInstance(getActivity(), getView()).loadDeadlineEditPage(listOfCourses.get(which), -1);
                                    }
                                })
                                .show();
                    } else {
                        new MaterialDialog.Builder(getActivity())
                                .title("No Courses Added")
                                .content("Please add a new course.")
                                .positiveText("ADD COURSE")
                                .negativeText("BACK")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog dialog, DialogAction which) {
                                        ((MainActivity) getActivity()).setSelected("addCourses");
                                        ((MainActivity) getActivity()).updateFragment();
                                        ((MainActivity) getActivity()).getDrawer().setSelection(
                                                ((MainActivity) getActivity()).getDrawer().getDrawerItem("addCourses")
                                        );
                                    }
                                })
                                .show();
                    }
                }
            }
        });
    }
}
