package com.comp4901e.attaho.timetable.data;

import java.util.Comparator;

/**
 * Created by attaho on 23/5/2016.
 */
public class TimeslotComparator implements Comparator<Timeslot> {
    @Override
    public int compare(Timeslot o1, Timeslot o2) {
        return o1.getStart().compareTo(o2.getStart());
    }
}
