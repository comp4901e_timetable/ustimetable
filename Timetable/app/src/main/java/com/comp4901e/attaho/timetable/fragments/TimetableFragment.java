package com.comp4901e.attaho.timetable.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.listeners.RecyclerItemClickListener;
import com.dexafree.materialList.view.MaterialListView;
import com.comp4901e.attaho.timetable.cards.SingleLineCardProvider;
import com.comp4901e.attaho.timetable.data.DataController;
import com.comp4901e.attaho.timetable.MainActivity;
import com.comp4901e.attaho.timetable.R;
import com.comp4901e.attaho.timetable.data.Timeslot;
import com.comp4901e.attaho.timetable.cards.TimetableCardProvider;
import com.comp4901e.attaho.timetable.cards.TimetableDayLabelProvider;

import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.neotech.library.retainabletasks.providers.TaskFragmentCompat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;

/**
 * A placeholder fragment containing a simple view.
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
public class TimetableFragment extends TaskFragmentCompat implements WeekView.EventClickListener, MonthLoader.MonthChangeListener {

    MaterialListView mListView;
    WeekView mWeekView;
    int nextClass;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Timetable");
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.timetable, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.timetable_actions, menu);
        mListView = (MaterialListView) getView().findViewById(R.id.timetable_listview);
        mListView.setVisibility(View.VISIBLE);
        mListView.getAdapter().clearAll();
        nextClass = 0;
        int positionCounter = 0;

        LocalDateTime todayDateTime = new LocalDateTime();

        // For testing
        //todayDateTime.plusDays(1);

        int todayDay = todayDateTime.getDayOfWeek();
        LocalTime todayTime = todayDateTime.toLocalTime();
        boolean isNextClassSet = false;

        DataController dc = DataController.getInstance(getActivity(), getView());
        TreeMap<Integer, ArrayList<Timeslot> > tstm = dc.getAllTimeslots();
        HashMap<String, HashMap<String, ArrayList<String>>> sectionHm = dc.getSectionDetailsMap();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("hh:mmaa").withLocale(Locale.US);
        if(tstm != null) {
            Card firstCard = null;
            String firstCardSectionNo = "";
            for(int i = 1; i <= 7; i++) {
                if(tstm.containsKey(i)) {
                    Card dayLabel = new Card.Builder(getActivity())
                            .withProvider(new TimetableDayLabelProvider())
                            .setDay(dc.getEEEFromDay(i))
                            .setBackgroundColor(getActivity().getResources().getColor(R.color.material_drawer_primary_light))
                            .endConfig()
                            .build();
                    dayLabel.setTag("dayLabel");
                    positionCounter++;
                    mListView.getAdapter().add(dayLabel);

                    for(Timeslot ts : tstm.get(i)) {
                        String fullName = sectionHm.get(ts.getSectionNo()).get("fullName").get(0);
                        String courseCode = fullName.substring(0, fullName.indexOf("-") - 1);
                        String section = sectionHm.get(ts.getSectionNo()).get("section").get(0);
                        String location = sectionHm.get(ts.getSectionNo()).get("room").get(0);
                        if(location.contains("(")) {
                            location = location.substring(0, location.indexOf("(")-1);
                        }
                        Card card = new Card.Builder(getActivity())
                                .withProvider(new TimetableCardProvider())
                                .setIconColor(getResources().getColor(R.color.material_drawer_primary_icon))
                                .setSection(courseCode + " - " + section)
                                .setDateTime(
                                        fmt.print(ts.getStart()).toUpperCase() + " - " + fmt.print(ts.getEnd()).toUpperCase()
                                )
                                .setLocation(location)
                                .setOtherClass()
                                .endConfig()
                                .build();

                        if(i == todayDay && ts.getStart().isAfter(todayTime) && !isNextClassSet) {
                            nextClass = positionCounter;
                            setNextClassCard(card, ts.getSectionNo());
                            isNextClassSet = true;
                        } else if(i > todayDay && !isNextClassSet) {
                            nextClass = positionCounter;
                            setNextClassCard(card, ts.getSectionNo());
                            isNextClassSet = true;
                        }
                        card.setTag(fullName);
                        mListView.getAdapter().add(card);
                        if(firstCard == null) {
                            firstCard = card;
                            firstCardSectionNo = ts.getSectionNo();
                        }
                        positionCounter++;
                    }
                }
            }
            if(!isNextClassSet) {
                setNextClassCard(firstCard, firstCardSectionNo);
            }
        }

        if (mListView.getAdapter().getItemCount() == 0) {
            Card singleLineCard = new Card.Builder(getActivity())
                    .withProvider(new SingleLineCardProvider())
                    .setText("Click here to add courses")
                    .endConfig()
                    .build();
            singleLineCard.setTag("noTimetable");
            mListView.getAdapter().add(singleLineCard);
        }

        mListView.addOnItemTouchListener(new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(final Card view, int position) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String data = (String) view.getTag();
                        if (data.equals("dayLabel")) return;
                        if (data.equals("noTimetable")) {
                            ((MainActivity) getActivity()).setSelected("addCourses");
                            ((MainActivity) getActivity()).updateFragment();
                            ((MainActivity) getActivity()).getDrawer().setSelection(
                                    ((MainActivity) getActivity()).getDrawer().getDrawerItem("addCourses")
                            );
                            return;
                        }
                        if(((MainActivity) getActivity()) == null) return;
                        ((MainActivity) getActivity()).setSelected(data);
                        ((MainActivity) getActivity()).updateFragment();
                        ((MainActivity) getActivity()).getDrawer().setSelection(
                                ((MainActivity) getActivity()).getDrawer().getDrawerItem(data)
                        );
                    }
                }, 200);
            }

            @Override
            public void onItemLongClick(final Card view, int position) {

            }
        });

        mListView.smoothScrollToPosition(nextClass);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < mListView.getChildCount(); i++) {
                    if (mListView.getChildAt(i).getTag() == "nextClass") {
                        YoYo.with(Techniques.Shake)
                                .duration(700)
                                .playOn(mListView.getChildAt(i));
                    }
                }
            }
        }, 200);


        // Get a reference for the week view in the layout.
        mWeekView = (WeekView) getView().findViewById(R.id.weekView);
        mWeekView.setVisibility(View.GONE);

        // Set an action when any event is clicked.
        mWeekView.setOnEventClickListener(this);

        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.
        mWeekView.setMonthChangeListener(this);

        mWeekView.setDateTimeInterpreter(new DateTimeInterpreter() {
            @Override
            public String interpretDate(Calendar date) {
                return new SimpleDateFormat("EEE", Locale.US).format(date.getTime());
            }

            @Override
            public String interpretTime(int hour) {
                if (hour == 12) {
                    return "12PM";
                }
                return hour > 11 ? (hour - 12) + "PM" : (hour == 0 ? "12AM" : hour + "AM");
            }
        });
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Double hourHeight = display.getHeight()*0.075;
        mWeekView.setHourHeight(hourHeight.intValue());

        mWeekView.goToHour(8);
        mWeekView.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_day) {
            mListView.setVisibility(View.VISIBLE);
            mWeekView.setVisibility(View.GONE);
            mListView.smoothScrollToPosition(nextClass);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Do something after 5s = 5000ms
                    for(int i = 0; i < mListView.getChildCount(); i++) {
                        if(mListView.getChildAt(i).getTag() == "nextClass") {
                            YoYo.with(Techniques.Shake)
                                    .duration(700)
                                    .playOn(mListView.getChildAt(i));
                        }
                    }
                }
            }, 200);


            return true;
        }

        if (id == R.id.action_week) {
            mListView.setVisibility(View.GONE);
            WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Double hourHeight = display.getHeight() * 0.075;
            mWeekView.setHourHeight(hourHeight.intValue());
            mWeekView.goToHour(8);
            mWeekView.setVisibility(View.VISIBLE);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
        DataController dc = DataController.getInstance(getActivity(), getView());
        for(String s : dc.getCurrentTermCourses()){
            String courseCode = event.getName().substring(0, event.getName().indexOf("\n"));
            if(s.contains(courseCode)){
                ((MainActivity) getActivity()).setSelected(s);
                ((MainActivity) getActivity()).updateFragment();
                ((MainActivity) getActivity()).getDrawer().setSelection(
                        ((MainActivity) getActivity()).getDrawer().getDrawerItem(s)
                );
                break;
            }
        }
    }

    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        DataController dc = DataController.getInstance(getActivity(), getView());
        TreeMap<Integer, ArrayList<Timeslot> > tstm = dc.getAllTimeslots();
        HashMap<String, HashMap<String, ArrayList<String>>> sectionHm = dc.getSectionDetailsMap();
        // Populate the week view with some events.
        List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();

        if(tstm == null) return events;

        for(int i = 1; i <= 7; i++) {
            if(tstm.containsKey(i)) {
                for(Timeslot ts : tstm.get(i)) {
                    // Add an event for each timeslot

                    String fullName = sectionHm.get(ts.getSectionNo()).get("fullName").get(0);
                    String courseCode = fullName.substring(0, fullName.indexOf("-") - 1);
                    String section = sectionHm.get(ts.getSectionNo()).get("section").get(0);
                    String location = sectionHm.get(ts.getSectionNo()).get("room").get(ts.getIndex());
                    location = location.replace(", ", "\n");
                    if(location.contains("(")) {
                        location = location.substring(0, location.indexOf("(")-1);
                    }
                    if(location.contains("Lecture Theater ")) {
                        location = location.replace("Lecture Theater ", "LT-");
                    }

                    for(int week = 1; week <= 4; week++) {
                        Calendar startTime = Calendar.getInstance();
                        startTime.set(Calendar.WEEK_OF_MONTH, week);
                        startTime.set(Calendar.DAY_OF_WEEK, ts.getDay() == 7 ? 1 : ts.getDay() + 1);
                        startTime.set(Calendar.HOUR_OF_DAY, ts.getStart().getHourOfDay());
                        startTime.set(Calendar.MINUTE, ts.getStart().getMinuteOfHour());
                        startTime.set(Calendar.MONTH, newMonth - 1);
                        startTime.set(Calendar.YEAR, newYear);
                        Calendar endTime = (Calendar) startTime.clone();
                        endTime.set(Calendar.HOUR_OF_DAY, ts.getEnd().getHourOfDay());
                        endTime.set(Calendar.MINUTE, ts.getEnd().getMinuteOfHour());
                        endTime.set(Calendar.MONTH, newMonth - 1);
                        WeekViewEvent event = new WeekViewEvent(1, courseCode + "\n" + location, startTime, endTime);
                        event.setColor(getResources().getColor(R.color.material_drawer_primary_dark));
                        events.add(event);
                    }
                }
            }
        }

        return events;
    }

    public void setNextClassCard (Card card, String sn) {
        if(card == null) return;
        ((TimetableCardProvider) card.getProvider())
                .setNextClass()
                .setBackgroundColor(
                        getActivity().getResources().getColor(R.color.material_drawer_primary_dark))
                .endConfig();
        DataController.getInstance(getActivity(), getView()).setNextClass(sn);
        ((MainActivity)getActivity()).showNextClass();
    }
}
