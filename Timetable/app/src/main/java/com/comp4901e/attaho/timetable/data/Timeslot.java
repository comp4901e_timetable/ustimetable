package com.comp4901e.attaho.timetable.data;

import org.joda.time.LocalTime;

/**
 * Created by attaho on 21/5/2016.
 */
public class Timeslot {

    private String sectionNo;
    private int index;
    private int day;
    private LocalTime start;
    private LocalTime end;

    public String getSectionNo() {
        return sectionNo;
    }

    public void setSectionNo(String sectionNo) {
        this.sectionNo = sectionNo;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public LocalTime getStart() {
        return start;
    }

    public void setStart(LocalTime start) {
        this.start = start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public void setEnd(LocalTime end) {
        this.end = end;
    }
}
