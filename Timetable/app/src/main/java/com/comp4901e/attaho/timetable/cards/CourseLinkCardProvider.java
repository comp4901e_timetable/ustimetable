package com.comp4901e.attaho.timetable.cards;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.comp4901e.attaho.timetable.R;

/**
 * Created by attaho on 21/5/2016.
 */
public class CourseLinkCardProvider extends CardProvider<CourseLinkCardProvider> {
    public int getLinkIndex() {
        return linkIndex;
    }

    int linkIndex = -1;
    String fullName = null;
    String name = null;

    public String getUrl() {
        return url;
    }

    String url = null;

    boolean isLoading = false;

    public int getLayout() {
        return R.layout.course_link_view;
    }

    public CourseLinkCardProvider setLinkIndex(int linkIndex){
        this.linkIndex = linkIndex;
        notifyDataSetChanged();
        return this;
    }


    public CourseLinkCardProvider setFullName(String text){
        this.fullName = text;
        notifyDataSetChanged();
        return this;
    }

    public CourseLinkCardProvider setLinkName(String text){
        this.name = text;
        notifyDataSetChanged();
        return this;
    }

    public CourseLinkCardProvider setUrl(String text){
        this.url = text;
        notifyDataSetChanged();
        return this;
    }

    @Override
    public void render(@NonNull View view, @NonNull Card card) {
        super.render(view, card);

        TextView textView;
        View layoutView;

        textView = (TextView) view.findViewById(R.id.linkName);
        textView.setText(name);
    }

}
