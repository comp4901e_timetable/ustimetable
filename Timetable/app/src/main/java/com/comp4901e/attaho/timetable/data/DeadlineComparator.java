package com.comp4901e.attaho.timetable.data;

import com.dexafree.materialList.card.Card;
import com.comp4901e.attaho.timetable.cards.CourseDeadlineCardProvider;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by attaho on 23/5/2016.
 */
public class DeadlineComparator implements Comparator<Card> {
    @Override
    public int compare(Card o1, Card o2) {
        SimpleDateFormat format = new SimpleDateFormat("d MMMM yyyy  h:mm a");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse(((CourseDeadlineCardProvider) o1.getProvider()).getDate());
            d2 = format.parse(((CourseDeadlineCardProvider) o2.getProvider()).getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d1.compareTo(d2);
    }
}
