package com.comp4901e.attaho.timetable.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class DataUserDataMap {
    public HashMap<String, TreeMap<String, HashMap<String, ArrayList<Object> > > > getMap() {
        return map;
    }

    public void setMap(HashMap<String, TreeMap<String, HashMap<String, ArrayList<Object> > > > map) {
        this.map = map;
    }

    private HashMap<String, TreeMap<String, HashMap<String, ArrayList<Object> > > > map = null;
}
