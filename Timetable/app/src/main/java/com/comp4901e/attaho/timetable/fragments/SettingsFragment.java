package com.comp4901e.attaho.timetable.fragments;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.support.v4.preference.PreferenceFragmentCompat;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.comp4901e.attaho.timetable.MainActivity;
import com.comp4901e.attaho.timetable.R;
import com.comp4901e.attaho.timetable.data.DataController;
import com.comp4901e.attaho.timetable.data.VolleyRequestQueue;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    public void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Settings");
        addPreferencesFromResource(R.xml.prefs);

        final ListPreference listPreference = (ListPreference) findPreference("termId");
        setListPreferenceData(listPreference);
        listPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                setListPreferenceData(listPreference);
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
    {
        if (key.equals("termId"))
        {
            // get preference by key
            ListPreference listPref = (ListPreference) findPreference(key);
            DataController.getInstance(getActivity(),getView()).setCurrentTerm(listPref.getValue());
            DataController.getInstance(getActivity(), getView()).loadData();
            DataController.getInstance(getActivity(), getView()).updateCourses();
            ((MainActivity)getActivity()).updateDrawer();
            listPref.setSummary(DataController.getInstance(getActivity(), getView()).getCurrentTermText());
            // do your stuff here
        }
    }

    protected void setListPreferenceData(final ListPreference lp) {
        final ArrayList<String> entriesArr = new ArrayList<>();
        final ArrayList<String> entryValuesArr = new ArrayList<>();

        lp.setSummary(DataController.getInstance(getActivity(), getView()).getCurrentTermText());

        // Instantiate the RequestQueue.
        RequestQueue queue = VolleyRequestQueue.getInstance(getActivity()).getRequestQueue();
        String url ="https://w5.ab.ust.hk/wcq/cgi-bin/";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Document doc = Jsoup.parse(response);
                        int termSize = doc.select(".termselect > a").size();
                        CharSequence[] entries = new CharSequence[termSize];
                        CharSequence[] entryValues = new CharSequence[termSize];
                        int index = 0;
                        for (Element e : doc.select(".termselect > a")) {
                            entries[index] = e.text();
                            String html = e.outerHtml();
                            int indexOfArrow = html.indexOf(">");
                            entryValues[index] = html.substring(indexOfArrow - 6, indexOfArrow - 2);
                            index++;
                        }
                        lp.setEntries(entries);
                        lp.setEntryValues(entryValues);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        CharSequence[] entries = {"Please check your internet connection and try again."};
                        CharSequence[] entryValues = {lp.getValue()};
                        lp.setEntries(entries);
                        lp.setEntryValues(entryValues);
                    }
                });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
