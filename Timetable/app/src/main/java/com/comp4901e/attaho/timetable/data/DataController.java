package com.comp4901e.attaho.timetable.data;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import com.comp4901e.attaho.timetable.MainActivity;
import com.comp4901e.attaho.timetable.R;
import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.shawnlin.preferencesmanager.PreferencesManager;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.neotech.library.retainabletasks.Task;
import org.neotech.library.retainabletasks.providers.TaskFragmentCompat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import java.util.TreeMap;

import okhttp3.OkHttpClient;

public class DataController extends TaskFragmentCompat implements Task.Callback, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private static DataController mInstance;

    private Activity activity;

    public void setActivity(Activity a) {
        activity = a;
    }

    private View view;

    public void setView(View v) {
        view = v;
    }

    public HashMap<String, HashMap<String, ArrayList<String>>> getSectionDetailsMap() {
        return sectionDetailsMap.get(currentTerm);
    }

    private HashMap<String, HashMap<String, HashMap<String, ArrayList<String> > > >sectionDetailsMap = new HashMap<>();
    private HashMap<String, TreeMap<String, HashMap<String, ArrayList<String> > > >courses = new HashMap<>();
    private HashMap<String, TreeMap<String, HashMap<String, ArrayList<Object> > > >userDataMap = new HashMap<>();

    public TreeMap<String, HashMap<String, ArrayList<Object>>> getUserDataMap() {
        return userDataMap.get(currentTerm);
    }

    private ArrayList<String> dept = new ArrayList<String>();
    private Task.Callback tc = this;
    private int todo;

    ArrayAdapter adapter;

    private Calendar calendar;
    private TextView deadlineDateText;

    private int touchPositionY = 0;

    private MaterialDialog progress;

    private String nextClass = "";

    private OkHttpClient client = new OkHttpClient();

    private String currentTerm = "1530";

    private String currentTermText = "2015-16 Spring";

    private TreeMap<String, String> terms = new TreeMap<>();

    public String getCurrentTerm() {
        return currentTerm;
    }

    public void setCurrentTerm(String semester) {
        this.currentTerm = semester;
    }

    private String courseUrl = "https://w5.ab.ust.hk/wcq/cgi-bin/";

    private synchronized void insertCourse(String k, HashMap<String, ArrayList<String> > v) {
        courses.get(currentTerm).put(k, v);
    }

    private synchronized void insertDetails(String k, HashMap<String, ArrayList<String> > v) {
        sectionDetailsMap.get(currentTerm).put(k, v);
    }

    private synchronized void decrementTodo() {
        todo--;
    }

    public String getNextClass() {
        return nextClass;
    }

    public void setNextClass(String nextClass) {
        this.nextClass = nextClass;
    }

    public String getCurrentTermText() {
        return currentTermText;
    }

    private class courseRequest extends StringRequest {

        public courseRequest(String url) {
            super(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            processDepts pd = new processDepts("GettingCourseDepts", response);
                            getTaskManager().execute(pd, tc);
                        }
                    },new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            // Error handling
                            System.out.println("Something went wrong!");
                            progress.dismiss();
                            new MaterialDialog.Builder(activity)
                                    .title("Oops!")
                                    .content("Please check your internet connection and try again.")
                                    .positiveText("BACK")
                                    .show();
                            error.printStackTrace();

                        }
                    }
            );
        }
    }

    private class courseRequestHelper extends StringRequest {

        public courseRequestHelper(String url, final String d) {
            super(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            processCourses pc = new processCourses("GettingCoursesOf-" + d, response);
                            getTaskManager().execute(pc, tc);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            // Error handling
                            System.out.println("Something went wrong!");
                            progress.dismiss();
                            new MaterialDialog.Builder(activity)
                                    .title("Oops!")
                                    .content("Please check your internet connection and try again.")
                                    .positiveText("BACK")
                                    .show();
                            error.printStackTrace();

                        }
                    }
            );
        }
    }

    private class processDepts extends Task<Integer, String> {

        private String response;

        public processDepts(String tag, String r){
            super(tag);
            response = r;
        }

        protected String doInBackground() {
            Document doc = Jsoup.parse(response);
            for (Element e : doc.select(".depts > a")) {
                dept.add(e.text());
            }
            todo = dept.size();
            for (String d : dept) {
                VolleyRequestQueue.getInstance(activity).addToRequestQueue(new courseRequestHelper(courseUrl + currentTerm + "/subject/" + d, d));
            }
            return "LoadDialog";
        }

    }

    private class processCourses extends Task<Integer, String> {

        private String response;

        public processCourses(String tag, String r){
            super(tag);
            response = r;
        }

        protected String doInBackground() {
            Document doc = Jsoup.parse(response);
            Elements e = doc.select(".course");
            for (int i = 0; i < e.size(); i++) {
                String fullName = e.get(i).select("h2").text();
                Elements sections = e.get(i).select(".sections").select(".newsect");
                HashMap<String, ArrayList<String> > sectionsMap = new HashMap<>();
                sectionsMap.put("LA", new ArrayList<String>());
                sectionsMap.put("L", new ArrayList<String>());
                sectionsMap.put("T", new ArrayList<String>());
                sectionsMap.put("R", new ArrayList<String>());
                for (int j = 0; j < sections.size(); j++) {
                    String section = sections.get(j).select("td").get(0).text();
                    String type = "";
                    if(section.contains("LA")) {
                        type = "LA";
                    } else if(section.contains("L")) {
                        type = "L";
                    } else if(section.contains("T")) {
                        type = "T";
                    } else if(section.contains("R")) {
                        type = "R";
                    }
                    sectionsMap.get(type).add(section.substring(section.indexOf("(")+1, section.indexOf(")")));
                }
                insertCourse(fullName, sectionsMap);


                sections = e.get(i).select(".sections").select("tr");
                String sectionNo = "";
                HashMap<String, ArrayList<String> > sectionNoMap = null;
                for (int j = 1; j < sections.size(); j++) {
                    Elements section = sections.get(j).select("td");
                    if(section.size() > 3) {
                        String sectionTemp = section.get(0).text();
                        if(!sectionNo.equals("")) {
                            insertDetails(sectionNo, sectionNoMap);
                        }
                        sectionNo = sectionTemp.substring(sectionTemp.indexOf("(") + 1, sectionTemp.indexOf(")"));
                        sectionNoMap = new HashMap<String, ArrayList<String> >();
                        sectionNoMap.put("fullName", new ArrayList<String>());
                        sectionNoMap.put("section", new ArrayList<String>());
                        sectionNoMap.put("dateTime", new ArrayList<String>());
                        sectionNoMap.put("room", new ArrayList<String>());

                        sectionNoMap.get("fullName").add(fullName);
                        sectionNoMap.get("section").add(section.get(0).text().substring(0, sectionTemp.indexOf(" ")));
                        sectionNoMap.get("dateTime").add(section.get(1).text());
                        sectionNoMap.get("room").add(section.get(2).text());
                    } else {
                        sectionNoMap.get("dateTime").add(section.get(0).text());
                        sectionNoMap.get("room").add(section.get(1).text());
                    }
                }
                insertDetails(sectionNo, sectionNoMap);


            }
            decrementTodo();
            return "Done";
        }
    }

    // UI Dialog for updating course information
    private void updatingCourses(){
        if(progress == null) {
            progress = new MaterialDialog.Builder(activity)
                    .title("Updating Courses")
                    .progressNumberFormat("")
                    .progress(false, todo, true)
                    .cancelable(false)
                    .show();
        }
        progress.setMaxProgress(todo);
        progress.show();


//        if(progress == null) {
//            progress=new ProgressDialog(activity);
//        }
//        progress.setMessage("Updating Courses");
//        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//        progress.setIndeterminate(false);
//        progress.setCancelable(false);
//        progress.setProgressNumberFormat(null);
//        progress.setProgress(0);
//        progress.setMax(todo);
//        progress.show();

        final int totalProgressTime = todo;
        final Thread t = new Thread() {
            @Override
            public void run() {
                while(todo > 0) {
                    try {
                        sleep(100);
                        progress.setProgress(totalProgressTime - todo);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                progress.dismiss();
            }
        };
        t.start();
    }

    public DataController() {

    }

    public static synchronized DataController getInstance(Activity a, View v) {
        if (mInstance == null) {
            mInstance = new DataController();
        }
        mInstance.setActivity(a);
        mInstance.setView(v);
        return mInstance;
    }

    public void updateCourses() {
        progress = new MaterialDialog.Builder(activity)
                .title("Updating Courses")
                .progressNumberFormat("")
                .progress(false, 150, true)
                .cancelable(false)
                .show();

        if(!sectionDetailsMap.containsKey(currentTerm)) {
            sectionDetailsMap.put(currentTerm, new HashMap<String, HashMap<String, ArrayList<String> > >());
        } else {
            sectionDetailsMap.get(currentTerm).clear();
        }

        if(!courses.containsKey(currentTerm)) {
            courses.put(currentTerm, new TreeMap<String, HashMap<String, ArrayList<String> > >());
        } else {
            courses.get(currentTerm).clear();
        }

        if(!userDataMap.containsKey(currentTerm)) {
            userDataMap.put(currentTerm, new TreeMap<String, HashMap<String, ArrayList<Object> > >());
        }

        VolleyRequestQueue.getInstance(activity).addToRequestQueue(new courseRequest(courseUrl + currentTerm + "/"));
        return;
    }

    public void saveData() {
        // Set up Preference Manager
        new PreferencesManager(activity).setName("prefs").init();

        // Save courses
        DataCourseTreeMap courseWrapper = new DataCourseTreeMap();
        courseWrapper.setMap(courses);
        PreferencesManager.putObject("courses", courseWrapper);

        // Save sections
        DataSectionDetailsMap sectionDetailsMapWrapper = new DataSectionDetailsMap();
        sectionDetailsMapWrapper.setMap(sectionDetailsMap);
        PreferencesManager.putObject("sectionDetailsMap", sectionDetailsMapWrapper);

        // Save user data
        DataUserDataMap userDataWrapper = new DataUserDataMap();
        userDataWrapper.setMap(userDataMap);
        PreferencesManager.putObject("userDataMap", userDataWrapper);

        return;
    }

    public void loadData() {
        SharedPreferences myPreference= PreferenceManager.getDefaultSharedPreferences(activity);
        String termId = myPreference.getString("termId", "1530");
        currentTerm = termId;
        int year1 = Integer.parseInt(termId.substring(0, 2));
        int year2 = Integer.parseInt(termId.substring(0, 2))+1;
        currentTermText = "20" + year1 + "-20" + year2 + " ";
        if(termId.substring(2,4).equals("10")) {
            currentTermText += "Fall";
        } else if(termId.substring(2,4).equals("20")) {
            currentTermText += "Winter";
        } else if(termId.substring(2,4).equals("30")) {
            currentTermText += "Spring";
        } else if(termId.substring(2,4).equals("40")) {
            currentTermText += "Summer";
        }

        // Set up Preference Manager
        new PreferencesManager(activity).setName("prefs").init();

        // Load courses
        DataCourseTreeMap courseWrapper = PreferencesManager.getObject("courses", DataCourseTreeMap.class);
        if(courseWrapper != null) {
            courses = courseWrapper.getMap();
        }

        // Load sections
        DataSectionDetailsMap sectionDetailsMapWrapper = PreferencesManager.getObject("sectionDetailsMap", DataSectionDetailsMap.class);
        if(sectionDetailsMapWrapper != null) {
            sectionDetailsMap = sectionDetailsMapWrapper.getMap();
        }

        // Load user data
        DataUserDataMap userDataWrapper = PreferencesManager.getObject("userDataMap", DataUserDataMap.class);
        if(userDataWrapper != null) {
            userDataMap = userDataWrapper.getMap();
        }

        return;
    }

    public void loadViews() {
        if (courses.get(currentTerm) == null || sectionDetailsMap.get(currentTerm) == null) {
            updateCourses();
        }
        Object[] temp = courses.get(currentTerm).keySet().toArray();
        adapter = new CourseListAdapter(activity, Arrays.copyOf(temp, temp.length, String[].class));
        final EditText inputSearch = (EditText) view.findViewById(R.id.courseSearch);
        final ListView listView = (ListView) view.findViewById(R.id.mobile_list);
        if(listView == null) return;
        listView.setAdapter(adapter);
        listView.setFastScrollEnabled(true);
        listView.setFastScrollAlwaysVisible(true);
        listView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(inputSearch.getWindowToken(), 0);

                return false;
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(inputSearch.getWindowToken(), 0);
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(inputSearch.getWindowToken(), 0);

                Object o = listView.getItemAtPosition(position);
                final String fullName = (String) o;

                if (userDataMap.containsKey(currentTerm)) {
                    if (userDataMap.get(currentTerm).containsKey(fullName)) {
                        new MaterialDialog.Builder(activity)
                                .title(fullName.substring(0, fullName.indexOf("-") - 1))
                                .content("You have already added this course.")
                                .positiveText("BACK")
                                .neutralText("GO TO COURSE PAGE")
                                .onNeutral(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog dialog, DialogAction which) {
                                        ((MainActivity) getActivity()).setSelected(fullName);
                                        ((MainActivity) getActivity()).updateFragment();
                                        ((MainActivity) getActivity()).getDrawer().setSelection(
                                                ((MainActivity) getActivity()).getDrawer().getDrawerItem(fullName)
                                        );
                                    }
                                })
                                .show();
                        return;
                    }
                }
                loadEditCourseOverlay(fullName);
            }
        });

        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });
    }

    @Override
    public void onPreExecute(Task<?, ?> task) {
        //Task started
    }

    @Override
    public void onPostExecute(Task<?, ?> task) {
        if (task.getResult() == "LoadDialog") {
            updatingCourses();
        }
        if (todo == 0 && activity != null) {
            saveData();
            loadViews();
        }
    }

    @Override
    public Task.Callback onPreAttach(Task<?, ?> task) {
        //Restore the user-interface based on the tasks state
        return this; //This Activity implements Task.Callback
    }

    public void addOverlayClose(View v) {
        final View view = v;
        view.animate()
                .translationX(activity.getWindowManager().getDefaultDisplay().getWidth())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        if (view.getTranslationX() != activity.getWindowManager().getDefaultDisplay().getWidth()) return;
                        view.setTranslationX(0);
                        view.setVisibility(View.GONE);
                    }
                });
    }

    public Set<String> getCurrentTermCourses() {
        if(!userDataMap.containsKey(currentTerm)) return null;
        return userDataMap.get(currentTerm).keySet();
    }

    public HashMap<String, ArrayList<Object>> getCourse(String fullName) {
        if(!userDataMap.containsKey(currentTerm)) return null;
        return userDataMap.get(currentTerm).get(fullName);
    }

    public ArrayAdapter getAdapter() {
        return adapter;
    }

    public void removeCourse(String fullName) {
        userDataMap.get(currentTerm).remove(fullName);
        saveData();
        ((MainActivity) activity).updateDrawer();
        ((MainActivity) activity).setSelected("addCourses");
        ((MainActivity) activity).updateFragment();
        Toast.makeText(activity, fullName.substring(0, fullName.indexOf("-") - 1) + " has been removed.", Toast.LENGTH_LONG).show();
    }

    public TreeMap<Integer, ArrayList<Timeslot> > getAllTimeslots() {
        TreeMap<Integer, ArrayList<Timeslot>> all = new TreeMap<>();
        if(!userDataMap.containsKey(currentTerm)) return null;
        for(String courseName : userDataMap.get(currentTerm).keySet()) {
            for(Object snObj : userDataMap.get(currentTerm).get(courseName).get("sections")) {
                String sn = (String) snObj;
                int index = 0;
                for(String dt : sectionDetailsMap.get(currentTerm).get(sn).get("dateTime")) {
                    if(dt.equals("TBA")) continue;
                    if(dt.indexOf(" ") > 8) continue;
                    String days = dt.substring(0, dt.indexOf(" "));
                    int numOfdays = days.length()/2;

                    String startTime = dt.substring(dt.indexOf(" ") + 1, dt.indexOf("-")-1);
                    String endTime = dt.substring(dt.indexOf("-") + 2, dt.length());
                    DateTimeFormatter fmt = DateTimeFormat.forPattern("hh:mmaa").withLocale(Locale.US);
                    LocalTime startLT = fmt.parseLocalTime(startTime);
                    LocalTime endLT = fmt.parseLocalTime(endTime);

                    for(int i = 0; i < numOfdays; i++) {
                        Timeslot ts = new Timeslot();
                        ts.setSectionNo(sn);
                        ts.setIndex(index);
                        ts.setDay(getDayFromEE(dt.substring(i * 2, i * 2 + 2)));
                        ts.setStart(startLT);
                        ts.setEnd(endLT);
                        if (!all.containsKey(ts.getDay())) {
                            all.put(ts.getDay(), new ArrayList<Timeslot>());
                        }
                        all.get(ts.getDay()).add(ts);

                    }
                    index++;
                }

            }
        }
        for(Integer day : all.keySet()) {
            Collections.sort(all.get(day), new TimeslotComparator());
        }
        return all;
    }

    public int getDayFromEE(String ee) {
        switch (ee) {
            case "Mo":
                return 1;
            case "Tu":
                return 2;
            case "We":
                return 3;
            case "Th":
                return 4;
            case "Fr":
                return 5;
            case "Sa":
                return 6;
            case "Su":
                return 7;
            default:
                return 0;
        }
    }

    public String getEEEFromDay(int day) {
        switch (day) {
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
            case 7:
                return "Sunday";
            default:
                return "";
        }
    }

    public void loadEditCourseOverlay(final String fullName) {
        HashMap<String, ArrayList<String>> hm = courses.get(currentTerm).get(fullName);

        // Set Overlay of Add Courses
        view = activity.getWindow().getDecorView().getRootView();
        final View addCourseView = view.findViewById(R.id.addCourseOverlay);

        // Set Course Name
        ((TextView) view.findViewById(R.id.course_name_text)).setText(fullName.substring(fullName.indexOf("-") + 2));

        ArrayList<String> listOfSections;

        final HashMap<String, Integer[]> typesOfSections = new HashMap<String, Integer[]>();
        typesOfSections.put("L", new Integer[]{
                R.id.lecture_select,
                R.id.lecture_text});
        typesOfSections.put("LA", new Integer[]{
                R.id.laboratory_select,
                R.id.laboratory_text});
        typesOfSections.put("T", new Integer[]{
                R.id.tutorial_select,
                R.id.tutorial_text});
        typesOfSections.put("R", new Integer[]{
                R.id.research_select,
                R.id.research_text});

        // Load L , LA, T and R
        for (final String type : typesOfSections.keySet()) {
            listOfSections = new ArrayList<String>();
            for (String s : hm.get(type)) {
                String sectionString = "";
                String sectionNumber = sectionDetailsMap.get(currentTerm).get(s).get("section").get(0);
                String location = sectionDetailsMap.get(currentTerm).get(s).get("room").get(0);
                if(location.contains("(")) {
                    location = location.substring(0, location.indexOf("(")-1);
                }
                sectionString += sectionNumber + " (" + s + ") - "+ location +"\n";
                for (String s1 : sectionDetailsMap.get(currentTerm).get(s).get("dateTime")) {
                    sectionString += s1 + "\n";
                }
                sectionString = sectionString.substring(0, sectionString.length() - 1);
                listOfSections.add(sectionString);
            }
            final ArrayList<String> selections = listOfSections;

            final TextView selectedSection = (TextView) view.findViewById(typesOfSections.get(type)[1]);
            View selector = (View) view.findViewById(typesOfSections.get(type)[0]);

            if (selections.size() > 0) {
                selector.setVisibility(View.VISIBLE);

                if (userDataMap.get(currentTerm).containsKey(fullName)) {
                    for(int i = 0; i < selections.size(); i++) {
                        for(Object sn : userDataMap.get(currentTerm).get(fullName).get("sections")) {
                            if(selections.get(i).contains(sn+"")) {
                                selectedSection.setText(selections.get(i));
                                break;
                            };
                        }
                    }
                } else {
                    selectedSection.setText(selections.get(0));
                }
            } else {
                selector.setVisibility(View.GONE);
                selectedSection.setText("");
            }

            selector.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    String typeText = "";
                    if(type.equals("L")) {
                        typeText = "Lecture";
                    } else if(type.equals("LA")) {
                        typeText = "Laboratory";
                    } else if(type.equals("T")) {
                        typeText = "Tutorial";
                    } else if(type.equals("R")) {
                        typeText = "Research";
                    }
                    new MaterialDialog.Builder(activity)
                            .title(typeText)
                            .items(selections)
                            .itemsCallback(new MaterialDialog.ListCallback() {
                                @Override
                                public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                    selectedSection.setText(text);
                                }
                            })
                            .show();
                }
            });
        }

        // Display overlay with animation
        addCourseView.setVisibility(View.VISIBLE);
        addCourseView.setTranslationX(activity.getWindowManager().getDefaultDisplay().getWidth());
        addCourseView.animate()
                .setInterpolator(new FastOutSlowInInterpolator())
                .setDuration(400L)
                .translationX(0);

        // Set up Overlay toolbar
        Toolbar addCourseToolbar =
                (Toolbar) activity.getWindow().getDecorView().getRootView().findViewById(R.id.addCourseToolbar);

        if (userDataMap.get(currentTerm).containsKey(fullName)) {
            addCourseToolbar.setTitle("Edit " + fullName.substring(0, fullName.indexOf("-") - 1));
        } else {
            addCourseToolbar.setTitle("Add " + fullName.substring(0, fullName.indexOf("-") - 1));
        }

        addCourseToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        addCourseToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                addOverlayClose(addCourseView);
            }
        });

        // Set up Action Icons
        addCourseToolbar.getMenu().clear();
        addCourseToolbar.inflateMenu(R.menu.edit_course_actions);//changed
        addCourseToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem arg0) {
                if (arg0.getItemId() == R.id.action_ok) {
                    View viewTemp = activity.getWindow().getDecorView().getRootView();
                    ArrayList<Object> sections = new ArrayList<Object>();
                    for (String type : typesOfSections.keySet()) {
                        TextView selected = (TextView) viewTemp.findViewById(typesOfSections.get(type)[1]);
                        String fullSectionName = (String) selected.getText();
                        if (fullSectionName != null && !fullSectionName.equals("")) {
                            sections.add(fullSectionName.substring(fullSectionName.indexOf("(") + 1, fullSectionName.indexOf(")")));
                        }
                    }

                    if (userDataMap.get(currentTerm).containsKey(fullName)) {
                        userDataMap.get(currentTerm).get(fullName).get("sections").clear();
                        userDataMap.get(currentTerm).get(fullName).get("sections").addAll(sections);
                        Toast.makeText(activity, fullName.substring(0, fullName.indexOf("-") - 1) + " has been updated.", Toast.LENGTH_LONG).show();
                    } else {
                        HashMap<String, ArrayList<Object>> newCourse = new HashMap<String, ArrayList<Object>>();
                        newCourse.put("sections", sections);
                        newCourse.put("deadlines", new ArrayList<Object>());
                        newCourse.put("links", new ArrayList<Object>());
                        userDataMap.get(currentTerm).put(fullName, newCourse);
                        Toast.makeText(activity, fullName.substring(0, fullName.indexOf("-") - 1) + " has been added.", Toast.LENGTH_LONG).show();
                    }
                    addOverlayClose(viewTemp.findViewById(R.id.addCourseOverlay));
                    ((MainActivity) activity).updateDrawer();
                    ((MainActivity) activity).setSelected(fullName);
                    ((MainActivity) activity).updateFragment();
                    saveData();
                }
                return false;
            }
        });
    }

    public void loadEditLinkPage(final String fullName, final int index) {

        // Set Overlay of Add Links
        view = activity.getWindow().getDecorView().getRootView();
        final View addLinkView = view.findViewById(R.id.addLinkOverlay);



        // Set Link Name TODO: Only for editing
        final EditText linkNameEdit = (EditText) view.findViewById(R.id.link_name);
        final EditText linkUrlEdit = (EditText) view.findViewById(R.id.link_url);
        if(index == -1) {
            linkNameEdit.setText("");
            linkUrlEdit.setText("");
        } else {
            ArrayList<String> linkInfo = (ArrayList<String>) userDataMap.get(currentTerm).get(fullName).get("links").get(index);
            linkNameEdit.setText(linkInfo.get(0));
            linkUrlEdit.setText(linkInfo.get(1));
        }
        //((TextView) view.findViewById(R.id.course_name_text)).setText(fullName.substring(fullName.indexOf("-") + 2));

        View linkNameLayout = view.findViewById(R.id.link_name_layout);
        linkNameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linkNameEdit.requestFocus();
                linkNameEdit.setSelection(linkNameEdit.getText().length());
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(linkNameEdit, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        View linkUrlLayout = view.findViewById(R.id.link_url_layout);
        linkUrlLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linkUrlEdit.requestFocus();
                linkUrlEdit.setSelection(linkUrlEdit.getText().length());
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(linkUrlEdit, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        // Load Name and URL TODO: When editing

        // Display overlay with animation
        addLinkView.setVisibility(View.VISIBLE);
        addLinkView.setTranslationX(activity.getWindowManager().getDefaultDisplay().getWidth());
        addLinkView.animate()
                .setInterpolator(new FastOutSlowInInterpolator())
                .setDuration(400L)
                .translationX(0);

        // Set up Overlay toolbar
        Toolbar addLinkToolbar =
                (Toolbar) activity.getWindow().getDecorView().getRootView().findViewById(R.id.addLinkToolbar);

        addLinkToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        addLinkToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                addOverlayClose(addLinkView);
            }
        });

        // Set up Action Icons
        addLinkToolbar.getMenu().clear();
        addLinkToolbar.inflateMenu(R.menu.edit_link_actions);//changed

        if (index == -1) {
            addLinkToolbar.setTitle("Add link");
            addLinkToolbar.getMenu().findItem(R.id.action_delete).setVisible(false);
            activity.invalidateOptionsMenu();
        } else {
            addLinkToolbar.setTitle("Edit link");
            view.findViewById(R.id.action_delete).setVisibility(View.VISIBLE);
            addLinkToolbar.getMenu().findItem(R.id.action_delete).setVisible(true);
            activity.invalidateOptionsMenu();
        }

        addLinkToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem arg0) {
                if (arg0.getItemId() == R.id.action_delete) {
                    new MaterialDialog.Builder(activity)
                            .title("Are you sure you want to delete this link?")
                            .positiveText("Delete")
                            .negativeText("Cancel")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    View viewTemp = activity.getWindow().getDecorView().getRootView();
                                    addOverlayClose(viewTemp.findViewById(R.id.addLinkOverlay));
                                    userDataMap.get(currentTerm).get(fullName).get("links").remove(index);
                                    // DataController.getInstance(getActivity(), getView()).removeCourse(courseName);
                                    saveData();
                                    loadData();
                                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                    ((MainActivity) activity).updateFragment();
                                }
                            })
                            .show();
                    return false;
                }
                if (arg0.getItemId() == R.id.action_ok) {
                    View viewTemp = activity.getWindow().getDecorView().getRootView();
                    if(linkNameEdit.getText().toString().equals("")) {
                        new MaterialDialog.Builder(activity)
                                .title("Oops!")
                                .content("Please enter link name.")
                                .positiveText("BACK")
                                .show();
                        return false;
                    }
                    if(linkUrlEdit.getText().toString().equals("")) {
                        new MaterialDialog.Builder(activity)
                                .title("Oops!")
                                .content("Please enter link URL.")
                                .positiveText("BACK")
                                .show();
                        return false;
                    }
                    if(index == -1) {
                        String[] link = new String[2];
                        link[0] = linkNameEdit.getText().toString();
                        link[1] = linkUrlEdit.getText().toString();
                        userDataMap.get(currentTerm).get(fullName).get("links").add(link);
                        Toast.makeText(activity, linkNameEdit.getText().toString() + " has been added.", Toast.LENGTH_LONG).show();
                    } else {
                        String[] link = new String[2];
                        ArrayList<String> linkData = (ArrayList<String>)userDataMap.get(currentTerm).get(fullName).get("links").get(index);
                        linkData.set(0, linkNameEdit.getText().toString());
                        linkData.set(1, linkUrlEdit.getText().toString());
                        Toast.makeText(activity, linkNameEdit.getText().toString() + " has been updated.", Toast.LENGTH_LONG).show();
                    }
                    addOverlayClose(viewTemp.findViewById(R.id.addLinkOverlay));
                    saveData();
                    loadData();
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    ((MainActivity) activity).updateFragment();
                }
                return false;
            }
        });
    }

    public void loadDeadlineEditPage(final String fullName, final int index) {
        calendar = Calendar.getInstance();

        // Set Overlay of Add Links
        view = activity.getWindow().getDecorView().getRootView();
        final View addDeadlineView = view.findViewById(R.id.addDeadlineOverlay);



        // Set Link Name TODO: Only for editing
        final EditText deadlineNameEdit = (EditText) view.findViewById(R.id.deadline_name);
        deadlineDateText = (TextView) view.findViewById(R.id.deadline_date);
        if(index == -1) {
            deadlineNameEdit.setText("");
            SimpleDateFormat format = new SimpleDateFormat("d MMMM yyyy  h:mm a", Locale.US);
            deadlineDateText.setText(format.format(calendar.getTime()).replace("am", "AM").replace("pm", "PM"));
        } else {
            ArrayList<String> linkInfo = (ArrayList<String>) userDataMap.get(currentTerm).get(fullName).get("deadlines").get(index);
            deadlineNameEdit.setText(linkInfo.get(0));
            deadlineDateText.setText(linkInfo.get(1));
        }
        //((TextView) view.findViewById(R.id.course_name_text)).setText(fullName.substring(fullName.indexOf("-") + 2));

        View linkNameLayout = view.findViewById(R.id.deadline_name_layout);
        linkNameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deadlineNameEdit.requestFocus();
                deadlineNameEdit.setSelection(deadlineNameEdit.getText().length());
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(deadlineNameEdit, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        View linkUrlLayout = view.findViewById(R.id.deadline_date_layout);
        linkUrlLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dp = DatePickerDialog.newInstance(
                        DataController.getInstance(activity, view),
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                dp.setFirstDayOfWeek(Calendar.MONDAY);
                dp.show(((MainActivity) activity).getSupportFragmentManager(), "datePicker");
            }
        });

        // Load Name and URL TODO: When editing

        // Display overlay with animation
        addDeadlineView.setVisibility(View.VISIBLE);
        addDeadlineView.setTranslationX(activity.getWindowManager().getDefaultDisplay().getWidth());
        addDeadlineView.animate()
                .setInterpolator(new FastOutSlowInInterpolator())
                .setDuration(400L)
                .translationX(0);

        // Set up Overlay toolbar
        Toolbar addDeadlineToolbar =
                (Toolbar) activity.getWindow().getDecorView().getRootView().findViewById(R.id.addDeadlineToolbar);

        addDeadlineToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        addDeadlineToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                addOverlayClose(addDeadlineView);
            }
        });

        // Set up Action Icons
        addDeadlineToolbar.getMenu().clear();
        addDeadlineToolbar.inflateMenu(R.menu.edit_deadline_actions);//changed

        if (index == -1) {
            addDeadlineToolbar.setTitle("Add Deadline");
            addDeadlineToolbar.getMenu().findItem(R.id.action_delete).setVisible(false);
            activity.invalidateOptionsMenu();
        } else {
            addDeadlineToolbar.setTitle("Edit Deadline");
            view.findViewById(R.id.action_delete).setVisibility(View.VISIBLE);
            addDeadlineToolbar.getMenu().findItem(R.id.action_delete).setVisible(true);
            activity.invalidateOptionsMenu();
        }

        addDeadlineToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem arg0) {
                if (arg0.getItemId() == R.id.action_delete) {
                    new MaterialDialog.Builder(activity)
                            .title("Are you sure you want to delete this deadline?")
                            .positiveText("Delete")
                            .negativeText("Cancel")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    View viewTemp = activity.getWindow().getDecorView().getRootView();
                                    addOverlayClose(viewTemp.findViewById(R.id.addDeadlineOverlay));
                                    userDataMap.get(currentTerm).get(fullName).get("deadlines").remove(index);
                                    saveData();
                                    loadData();
                                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                    ((MainActivity) activity).updateFragment();
                                }
                            })
                            .show();
                    return false;
                }
                if (arg0.getItemId() == R.id.action_ok) {
                    View viewTemp = activity.getWindow().getDecorView().getRootView();
                    if (deadlineNameEdit.getText().toString().equals("")) {
                        new MaterialDialog.Builder(activity)
                                .title("Oops!")
                                .content("Please enter deadline name.")
                                .positiveText("BACK")
                                .show();
                        return false;
                    }
                    if (deadlineDateText.getText().toString().equals("")) {
                        new MaterialDialog.Builder(activity)
                                .title("Oops!")
                                .content("Please enter deadline date and time.")
                                .positiveText("BACK")
                                .show();
                        return false;
                    }
                    if (index == -1) {
                        String[] deadline = new String[2];
                        deadline[0] = deadlineNameEdit.getText().toString();
                        deadline[1] = deadlineDateText.getText().toString().replace("am", "AM").replace("pm", "PM");
                        userDataMap.get(currentTerm).get(fullName).get("deadlines").add(deadline);
                        Toast.makeText(activity, deadlineNameEdit.getText().toString() + " has been added.", Toast.LENGTH_LONG).show();
                    } else {
                        String[] link = new String[2];
                        ArrayList<String> deadlineData = (ArrayList<String>) userDataMap.get(currentTerm).get(fullName).get("deadlines").get(index);
                        deadlineData.set(0, deadlineNameEdit.getText().toString());
                        deadlineData.set(1, deadlineDateText.getText().toString().replace("am", "AM").replace("pm", "PM"));
                        Toast.makeText(activity, deadlineNameEdit.getText().toString() + " has been updated.", Toast.LENGTH_LONG).show();
                    }
                    addOverlayClose(viewTemp.findViewById(R.id.addDeadlineOverlay));
                    saveData();
                    loadData();
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    ((MainActivity) activity).updateFragment();
                }
                return false;
            }
        });
    }

    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        calendar.set(year, monthOfYear, dayOfMonth);
        TimePickerDialog tp = TimePickerDialog.newInstance(
                DataController.getInstance(activity, view),
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true);
        tp.show(((MainActivity) activity).getSupportFragmentManager(), "timePicker");
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        SimpleDateFormat format = new SimpleDateFormat("d MMMM yyyy  h:mm a", Locale.US);
        deadlineDateText.setText(format.format(calendar.getTime()).replace("am", "AM").replace("pm", "PM"));
    }
}