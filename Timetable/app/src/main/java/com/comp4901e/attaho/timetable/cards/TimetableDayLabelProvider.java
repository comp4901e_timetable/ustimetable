package com.comp4901e.attaho.timetable.cards;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.comp4901e.attaho.timetable.R;

/**
 * Created by attaho on 21/5/2016.
 */
public class TimetableDayLabelProvider extends CardProvider<TimetableDayLabelProvider> {
    String day;

    public int getLayout() {
        return R.layout.timetable_card_day_view;
    }

    public TimetableDayLabelProvider setDay(String text){
        this.day = text;
        notifyDataSetChanged();
        return this;
    }

    @Override
    public void render(@NonNull View view, @NonNull Card card) {
        super.render(view, card);

        TextView textView;

        textView = (TextView) view.findViewById(R.id.dayLabel);
        textView.setText(day);
    }

}
