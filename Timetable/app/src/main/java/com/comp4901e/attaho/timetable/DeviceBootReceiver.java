package com.comp4901e.attaho.timetable;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.util.Calendar;

/**
 * Created by attaho on 25/5/2016.
 */



public class DeviceBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Calendar cal = Calendar.getInstance();

            Intent alarmIntent = new Intent(context, DeviceBootReceiver.class);
            PendingIntent pintent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);

            AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            // schedule for every 30 seconds
            alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 1000, pintent);
        }
    }
}
