package com.comp4901e.attaho.timetable.fragments;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.listeners.RecyclerItemClickListener;
import com.dexafree.materialList.view.MaterialListView;
import com.comp4901e.attaho.timetable.data.DataController;
import com.comp4901e.attaho.timetable.R;
import com.comp4901e.attaho.timetable.cards.CourseDeadlineCardProvider;
import com.comp4901e.attaho.timetable.cards.CourseDetailsCardProvider;
import com.comp4901e.attaho.timetable.cards.CourseLinkCardProvider;
import com.comp4901e.attaho.timetable.data.DeadlineComparator;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import org.neotech.library.retainabletasks.providers.TaskFragmentCompat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * A placeholder fragment containing a simple view.
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
public class CourseFragment extends TaskFragmentCompat {

    private String courseName;
    private HashMap<String, ArrayList<Object>> courseInfo;
    private boolean isLoading = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        courseName = this.getArguments().getString("courseName");
        courseInfo = DataController.getInstance(getActivity(), getView()).getCourse(courseName);
        getActivity().setTitle(courseName.substring(0, courseName.indexOf("-") - 1));
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.course, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.course_actions, menu);

        final DataController dc = DataController.getInstance(getActivity(), getView());
        HashMap<String, HashMap<String, ArrayList<String>>> sectionDetailsMap = dc.getSectionDetailsMap();

        String lecture = "";
        String laboratory = "";
        String tutorial = "";
        String research = "";

        for(Object sectionNum : courseInfo.get("sections")) {
            String sectionType = sectionDetailsMap.get(sectionNum).get("section").get(0);
            String sectionString = "";
            String sectionNumber = sectionDetailsMap.get(sectionNum).get("section").get(0);
            String location = sectionDetailsMap.get(sectionNum).get("room").get(0);
            if(location.contains("(")) {
                location = location.substring(0, location.indexOf("(")-1);
            }
            sectionString += sectionNumber + " (" + sectionNum + ") - "+ location +"\n";
            for (String s1 : sectionDetailsMap.get(sectionNum).get("dateTime")) {
                sectionString += s1 + "\n";
            }
            sectionString = sectionString.substring(0, sectionString.length() - 1);

            if(sectionType.contains("LA")) {
                laboratory += sectionString;
            } else if(sectionType.contains("L")) {
                lecture += sectionString;
            } else if(sectionType.contains("T")) {
                tutorial += sectionString;
            } else if(sectionType.contains("R")) {
                research += sectionString;
            }
        }


        MaterialListView mListView = (MaterialListView) getView().findViewById(R.id.course_listview);
        mListView.getAdapter().clearAll();
        Card card = new Card.Builder(getActivity())
                .withProvider(new CourseDetailsCardProvider())
                .setFullName(courseName)
                .setCourse(courseName.substring(courseName.indexOf("-") + 2))
                .setLecture(lecture)
                .setLaboratory(laboratory)
                .setTutorial(tutorial)
                .setResearch(research)
                .endConfig()
                .build();
        card.setTag("courseDetails");
        mListView.getAdapter().add(card);

        if(dc.getUserDataMap().get(courseName) != null) {
            int linkIndex = 0;
            for(Object o : dc.getUserDataMap().get(courseName).get("links")) {
                ArrayList<String> link = (ArrayList) o;
                Card linkCard = new Card.Builder(getActivity())
                        .withProvider(new CourseLinkCardProvider())
                        .setLinkIndex(linkIndex)
                        .setFullName(courseName)
                        .setLinkName(link.get(0))
                        .endConfig()
                        .build();
                linkCard.setTag(linkIndex+"courseLink-"+link.get(1));
                mListView.getAdapter().add(linkCard);
                linkIndex++;
            }
            int deadlineIndex = 0;
            ArrayList<Card> cardList = new ArrayList<>();
            for(Object o : dc.getUserDataMap().get(courseName).get("deadlines")) {
                ArrayList<String> deadline = (ArrayList) o;
                Card deadlineCard = new Card.Builder(getActivity())
                        .withProvider(new CourseDeadlineCardProvider())
                        .setDeadlineIndex(deadlineIndex)
                        .setFullName(courseName)
                        .setDeadlineName(deadline.get(0))
                        .setDate(deadline.get(1))
                        .endConfig()
                        .build();
                deadlineCard.setTag(deadlineIndex + "courseDeadline");
                cardList.add(deadlineCard);
                deadlineIndex++;
            }
            Collections.sort(cardList, new DeadlineComparator());
            Collections.reverse(cardList);
            for(Card c : cardList) {
                mListView.getAdapter().add(c);
            }
        }

        mListView.addOnItemTouchListener(new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(final Card view, int position) {
                String data = (String) view.getTag();
                if (data.equals("courseDetails")) {
                    if (isLoading) return;
                    isLoading = true;
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            DataController.getInstance(getActivity(), getView()).loadEditCourseOverlay(courseName);

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    isLoading = false;
                                }
                            }, 400);

                        }
                    }, 200);
                } else if (data.contains("courseLink")) {
                    String url = data.substring(data.indexOf("-")+1);
                    if (!url.startsWith("http://") && !url.startsWith("https://"))
                        url = "http://" + url;
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                } else if (data.contains("courseDeadline")) {
                    dc.loadDeadlineEditPage(courseName, Integer.parseInt(data.substring(0, 1)));
                }
            }

            @Override
            public void onItemLongClick(final Card view, int position) {
                String data = (String) view.getTag();
                if (data.contains("courseLink")) {
                    dc.loadEditLinkPage(courseName, Integer.parseInt(data.substring(0, 1)));
                } else if (data.contains("courseDeadline")) {
                    dc.loadDeadlineEditPage(courseName, Integer.parseInt(data.substring(0, 1)));
                }
            }
        });

        final FloatingActionMenu addMenu = (FloatingActionMenu) getView().findViewById(R.id.add_menu);


        FloatingActionButton addLink = (FloatingActionButton) getView().findViewById(R.id.add_link);
        addLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataController.getInstance(getActivity(), getView()).loadEditLinkPage(courseName, -1);
                addMenu.close(true);
            }
        });

        FloatingActionButton addDeadline = (FloatingActionButton) getView().findViewById(R.id.add_deadline);
        addDeadline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataController.getInstance(getActivity(), getView()).loadDeadlineEditPage(courseName, -1);
                addMenu.close(true);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_remove_course) {
            new MaterialDialog.Builder(getActivity())
                    .title("Are you sure you want to remove " + courseName.substring(0, courseName.indexOf("-") - 1) + "?")
                    .content("All related timetable, deadlines and links will be removed.")
                    .positiveText("Remove")
                    .negativeText("Cancel")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            DataController.getInstance(getActivity(), getView()).removeCourse(courseName);
                        }
                    })
                    .show();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }
}
